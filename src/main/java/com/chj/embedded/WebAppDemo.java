package com.chj.embedded;

import org.apache.catalina.startup.Tomcat;

/**
 * 要用嵌入式启动的方式启动一个SpringMVc的项目
 */
public class WebAppDemo {
    public static void main(String[] args) throws  Exception{
        Tomcat tomcat = new Tomcat();
        // tomcat.addWebapp("/ref","D:\\work_tomcat\\ref-comet");
        tomcat.addWebapp("hzcms_rest","D:\\java-tools\\apache-tomcat-8.5.42\\webapps\\hzcms_rest_war");
        tomcat.getConnector().setPort(80);
        tomcat.init();
        tomcat.start();
        // 用于阻塞Tomcat,等待请求过来
        tomcat.getServer().await();
    }
}
